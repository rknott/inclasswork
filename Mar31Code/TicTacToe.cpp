/**********************************************
* File: TicTacToe
* Author: Reilley Knott
* Email: rknott@nd.edu
* This is the main driver function that contains
* the algorithm will determine if somebody has won
* a game of tic tac toe on a board that has been input
**********************************************/

#include <iostream>
#define HEIGHT 3
#define WIDTH 3
using namespace std;

void hasWon (char, char[][HEIGHT]);

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* The main driver function for the program 
********************************************/
int main() {

	//Create model boards to be tested for winners
	char board1[WIDTH][HEIGHT];
	
	board1[0][0] = 'O';
	board1[0][1] = 'X';
	board1[0][2] = 'X';
	board1[1][0] = 'O';
	board1[1][1] = 'O';
	board1[1][2] = 'X';
	board1[2][0] = 'O';
	board1[2][1] = 'X';
	board1[2][2] = 'O';

	char board2[WIDTH][HEIGHT];
            
        board2[0][0] = 'X';
        board2[0][1] = 'X';
        board2[0][2] = 'X';
        board2[1][0] = 'O';
        board2[1][1] = 'X';
        board2[1][2] = 'O';
        board2[2][0] = 'X';
        board2[2][1] = 'O';
        board2[2][2] = 'O';
	
	char board3[WIDTH][HEIGHT];
            
        board3[0][0] = 'O';
        board3[0][1] = 'X';
        board3[0][2] = 'X';
        board3[1][0] = 'O';
        board3[1][1] = 'X';
        board3[1][2] = 'O';
        board3[2][0] = 'X';
        board3[2][1] = 'O';
        board3[2][2] = 'O';

	cout << "For Board 1: \n";
	hasWon ('O', board1);
	hasWon ('X', board1);
	cout << "For Board 2: \n";
	hasWon ('X', board2);
	hasWon ('O', board2);
	cout << "For Board 3: \n";
	hasWon ('X', board3);
	hasWon ('O', board3);

return 0;
}

/********************************************
* Function Name  : hasWon
* Pre-conditions : char x, char board[][HEIGHT]
* Post-conditions: none
* The file that will determine if the referenced
* char (either X or O) has gotten 3 in a row 
********************************************/
void hasWon (char x, char board[][HEIGHT]) {
	int count = 0;
	//Check Horizontal
	for (int j = 0; j <= HEIGHT-1; j++) {
		for (int i = 0; i <= WIDTH-1; i++) {
			if (board[i][j] != x) {
				count = 0;
				break;
			} else {
				count++;
				if (count == WIDTH) {
					cout << x << " has won horizontally!" << endl;
					return;
				}
			}
		}
	}

	//Check Vertical
	for (int i = 0; i <= WIDTH-1; i++) {
                for (int j = 0; j <= HEIGHT-1; j++) {
                        if (board[i][j] != x) {
                                count = 0;
                                break;
                        } else {
                                count++;
                                if (count == HEIGHT) {
                                        cout << x << " has won vertically!" << endl;
					return;
				}
                        }
                }
        }

	//Check Diagonals
	int j = 0;
	for (int i = 0; i <= HEIGHT-1; i++) {
		if (board[i][j] != x) {
                	count = 0;
                        break;
                        } else {
                                count++;
				j++;
                                if (count == WIDTH) {
                                        cout << x << " has won diagonally!" << endl;
					return;
                       		}
        		}
	
	}

	j = 0;
        for (int i = HEIGHT-1; i >= 0; i--) {
                if (board[i][j] != x) {
                        count = 0;
                        break;
                        } else {
                                count++;
                                j++;
                                if (count == WIDTH) {
                                        cout << x << " has won diagonally!" << endl;
                                        return;
                                }
                        }

        }

	cout << x << " did not win\n";
}	




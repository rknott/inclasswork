/**********************************************
* File: AVLAuthorTest.cpp
* Author: Reilley Knott
* Email: rknott@nd.edu
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

struct Author { 

	std::string firstName;
	std::string lastName;

	Author(std::string firstName, std::string lastName) : firstName(firstName), lastName(lastName) {}

	bool operator< (const Author& rhs) const { 
		
		if (lastName < rhs.lastName)
			return true;
		
			else if (lastName == rhs.lastName ) {
				if (firstName < rhs.firstName)
					return true;
			}

		return false; }

	bool operator== (const Author& rhs) const { 
		
		if(lastName != rhs.lastName)
			return false;
			else {
				if (firstName != rhs.firstName)
					return false;
					//return true;
			}

		return true; }

	friend std::ostream& operator<<(std::ostream& outStream, const Author& printAuth);
};

std::ostream& operator<<(std::ostream& outStream, const Author& printAuth) {

	outStream << printAuth.lastName << ", " << printAuth.firstName;

	return outStream;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
  AVLTree<Author> avlAuthor;

  Author Aardvark ("Aaron", "Aardvark");  
  Author Aardvark2 ("Gregory", "Aardvark");
  Author BadMean("BadMean", "Aardvark");

  avlAuthor.insert(Aardvark2);
  avlAuthor.insert(Aardvark);
  avlAuthor.insert(BadMean);

  avlAuthor.printTree();
  avlAuthor.remove(BadMean);

  std::cout << "----" << endl;

  avlAuthor.printTree();
	
    return 0;
}

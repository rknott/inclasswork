/****************************************
 * File name: SplayTree.h  
 * Author: Matthew Morrison 
 * Contact E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the class members and 
 * functions for an implementation of a 
 * Splay Tree.
 * *************************************/

#ifndef SPLAY_TREE_H
#define SPLAY_TREE_H

#include "DSExceptions.h"
#include <iostream> 
using namespace std;

template <typename T>
class SplayTree
{
  public:

    /**********************************************
     * Function Name:  SplayTree()
     * Pre-conditions: none
     * Post-conditions: none
     * 
     * Creates an empty SplayTree class with a null root
     * ********************************************/ 
    SplayTree( )
    {
        nullNode = new BinaryNode;
        nullNode->left = nullNode->right = nullNode;
        root = nullNode;
    }

    /**********************************************
     * Function Name:  SplayTree
     * Pre-conditions: const SplayTree & 
     * Post-conditions: none
     * 
     * This is the copy constructor for SplayTree 
     * ********************************************/  
    SplayTree( const SplayTree & rhs )
    {
        nullNode = new BinaryNode;
        nullNode->left = nullNode->right = nullNode;
        root = clone( rhs.root );
    }

    /**********************************************
     * Function Name:  SplayTree
     * Pre-conditions: SplayTree &&
     * Post-conditions: none
     * 
     * This is the move constructor for SplayTree
     * ********************************************/  
    SplayTree( SplayTree && rhs ) : root{ rhs.root }, nullNode{ rhs.nullNode }
    {
        rhs.root = nullptr;
        rhs.nullNode = nullptr;
    }

   /**********************************************
     * Function Name:  ~SplayTree
     * Pre-conditions: none
     * Post-conditions: none
     * 
     * This is the destructor for SplayTree
     * ********************************************/       
    ~SplayTree( )
    {
        makeEmpty( );
        delete nullNode;
    }

    
   /**********************************************
     * Function Name:  operator=
     * Pre-conditions: const SplayTree &
     * Post-conditions: SplayTree &
     * 
     * Copy assignment. Deep copy 
     *******************************************/
    SplayTree & operator=( const SplayTree & rhs )
    {
        SplayTree copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
        
   /**********************************************
     * Function Name:  operator=
     * Pre-conditions: SplayTree &&
     * Post-conditions: SplayTree &
     * 
     * Move assignment operator
     *******************************************/
    SplayTree & operator=( SplayTree && rhs )
    {
        std::swap( root, rhs.root );
        std::swap( nullNode, rhs.nullNode );
        
        return *this;
    }
    
   /**********************************************
     * Function Name:  findMin
     * Pre-conditions: none
     * Post-conditions: const T&
     * 
     * Find the smallest item in the tree.
     * Not the most efficient implementation (uses two passes), but has correct
     *     amortized behavior.
     * A good alternative is to first call find with parameter
     *     smaller than any item in the tree, then call findMin.
     * Return the smallest item or throw UnderflowException if empty.
     *********************************************/
    const T & findMin( )
    {
        if( isEmpty( ) )
            throw UnderflowException{ };

        BinaryNode *ptr = root;

        while( ptr->left != nullNode )
            ptr = ptr->left;

        splay( ptr->element, root );
        return ptr->element;
    }

   /**********************************************
     * Function Name:  findMax
     * Pre-conditions: none
     * Post-conditions: const T&
     * 
     * Find the largest item in the tree.
     * Not the most efficient implementation (uses two passes), but has correct
     *     amortized behavior.
     * A good alternative is to first call find with parameter
     *     larger than any item in the tree, then call findMax.
     * Return the largest item or throw UnderflowException if empty.
     ********************************************/
    const T & findMax( )
    {
        if( isEmpty( ) )
            throw UnderflowException{ };

        BinaryNode *ptr = root;

        while( ptr->right != nullNode )
            ptr = ptr->right;

        splay( ptr->element, root );
        return ptr->element;
    }

   /**********************************************
     * Function Name:  contains
     * Pre-conditions: const T&
     * Post-conditions: bool
     * 
     * Returns true if x is found in the tree.
     ******************************************/
    bool contains( const T & x )
    {
        if( isEmpty( ) )
            return false;
        splay( x, root );
        return root->element == x;
    }

   /**********************************************
     * Function Name:  isEmpty
     * Pre-conditions: none
     * Post-conditions: bool
     * 
     * Test if the tree is logically empty.
     * Return true if empty, false otherwise.
     ********************************************/
    bool isEmpty( ) const
    {
        return root == nullNode;
    }

    /**********************************************
     * Function Name:  operator<<
     * Pre-conditions: std::istream& , Bottle& 
     * Post-conditions: std::ostream& 
     * 
     * This is the overloaded << operator function
     * ********************************************/        
    friend std::ostream& operator<< (std::ostream& stream, const SplayTree& theSplay){
        
        if( theSplay.isEmpty( ) )
            stream << "Empty tree" << std::endl;
        else
            theSplay.printTree( theSplay.root , stream );
        
        return stream;
    }

   /**********************************************
     * Function Name:  makeEmpty
     * Pre-conditions: none
     * Post-conditions: bool
     * 
     * Make the tree logically empty.
     *********************************************/
    void makeEmpty( )
    {
    /******************************
     * Comment this out, because it is prone to excessive
     * recursion on degenerate trees. Use alternate algorithm.
        
        reclaimMemory( root );
        root = nullNode;
     *******************************/
        while( !isEmpty( ) )
        {
            findMax( );        // Splay max item to root
            remove( root->element );
        }
    }

   /**********************************************
     * Function Name:  insert
     * Pre-conditions: const T&
     * Post-conditions: void
     * 
     * Insert x into the tree; duplicates are ignored.
     *************************************************/
    void insert( const T & x )
    {
        static BinaryNode *newNode = nullptr;

        if( newNode == nullptr )
            newNode = new BinaryNode;
        newNode->element = x;

        if( root == nullNode )
        {
            newNode->left = newNode->right = nullNode;
            root = newNode;
        }
        else
        {
            splay( x, root );
            if( x < root->element )
            {
                newNode->left = root->left;
                newNode->right = root;
                root->left = nullNode;
                root = newNode;
            }
            else
            if( root->element < x )
            {
                newNode->right = root->right;
                newNode->left = root;
                root->right = nullNode;
                root = newNode;
            }
            else
                return;
        }
        newNode = nullptr;   // So next insert will call new
    }

   /**********************************************
     * Function Name:  remove
     * Pre-conditions: const T &
     * Post-conditions: none
     * 
     *  Remove x from the tree. Nothing is done if x is not found.
     ****************************************************/
    void remove( const T & x )
    {
            // If x is found, it will be splayed to the root by contains
        if( !contains( x ) )
            return;   // Item not found; do nothing

        BinaryNode *newTree;

        if( root->left == nullNode )
            newTree = root->right;
        else
        {
            // Find the maximum in the left subtree
            // Splay it to the root; and then attach right child
            newTree = root->left;
            splay( x, newTree );
            newTree->right = root->right;
        }
        delete root;
        root = newTree;
    }

private:

    struct BinaryNode
    {
        T  element;
        BinaryNode *left;
        BinaryNode *right;

        BinaryNode( ) : left{ nullptr }, right{ nullptr } { }
        
        BinaryNode( const T & theElement, BinaryNode *lt, BinaryNode *rt )
            : element{ theElement }, left{ lt }, right{ rt } { }       
    };
    
    BinaryNode *root;
    BinaryNode *nullNode;

   /**********************************************
     * Function Name:  reclaimMemory
     * Pre-conditions: BinaryNode *
     * Post-conditions: void
     * 
     * Internal method to reclaim internal nodes in subtree t.
     * WARNING: This is prone to running out of stack space.
     *******************************************/
    void reclaimMemory( BinaryNode * t )
    {
        if( t != t->left )
        {
            reclaimMemory( t->left );
            reclaimMemory( t->right );
            delete t;
        }
    }
    
   /**********************************************
     * Function Name:  printTree
     * Pre-conditions: BinaryNode *, std::ostream &
     * Post-conditions: void
     * 
     * Internal method to print a subtree t in sorted order.
     * WARNING: This is prone to running out of stack space.
     **********************************************/
   void printTree( BinaryNode *t, std::ostream &out ) const
    {
        if( t != t->left )
        {
            printTree( t->left, out );
            out << t->element << " " ;
            printTree( t->right, out );
        }
    }

   /**********************************************
     * Function Name:  clone
     * Pre-conditions: BinaryNode *
     * Post-conditions: BinaryNode *
     * 
     * Internal method to clone subtree.
     * WARNING: This is prone to running out of stack space.
     *********************************************/
    BinaryNode * clone( BinaryNode * t ) const
    {
        if( t == t->left )  // Cannot test against nullNode!!!
            return nullNode;
        else
            return new BinaryNode{ t->element, clone( t->left ), clone( t->right ) };
    }

   /**********************************************
     * Function Name:  rotateWithRightChild
     * Pre-conditions: BinaryNode * &
     * Post-conditions: void
     * 
     * Tree manipulations: Zig with Left Child
     * ******************************************/
    void rotateWithLeftChild( BinaryNode * & k2 )
    {
        BinaryNode *k1 = k2->left;
        k2->left = k1->right;
        k1->right = k2;
        k2 = k1;
    }

   /**********************************************
     * Function Name:  rotateWithRightChild
     * Pre-conditions: BinaryNode * &
     * Post-conditions: void
     * 
     * Tree manipulations: Zig with Right Child
     * ******************************************/
    void rotateWithRightChild( BinaryNode * & k1 )
    {
        BinaryNode *k2 = k1->right;
        k1->right = k2->left;
        k2->left = k1;
        k1 = k2;
    }

   /**********************************************
     * Function Name:  splay
     * Pre-conditions: const T &, BinaryNode * &
     * Post-conditions: void
     * 
     * Internal method to perform a top-down splay.
     * The last accessed node becomes the new root.
     * This method may be overridden to use a different
     * splaying algorithm, however, the splay tree code
     * depends on the accessed item going to the root.
     * x is the target item to splay around.
     * t is the root of the subtree to splay.
     **************************************************/
    void splay( const T & x, BinaryNode * & t )
    {
        BinaryNode *leftTreeMax, *rightTreeMin;
        static BinaryNode header;

        header.left = header.right = nullNode;
        leftTreeMax = rightTreeMin = &header;

        nullNode->element = x;   // Guarantee a match

        for( ; ; )
            if( x < t->element )
            {
                if( x < t->left->element )
                    rotateWithLeftChild( t );
                if( t->left == nullNode )
                    break;
                // Link Right
                rightTreeMin->left = t;
                rightTreeMin = t;
                t = t->left;
            }
            else if( t->element < x )
            {
                if( t->right->element < x )
                    rotateWithRightChild( t );
                if( t->right == nullNode )
                    break;
                // Link Left
                leftTreeMax->right = t;
                leftTreeMax = t;
                t = t->right;
            }
            else
                break;

        leftTreeMax->right = t->left;
        rightTreeMin->left = t->right;
        t->left = header.right;
        t->right = header.left;
    }
};

#endif
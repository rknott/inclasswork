/**********************************************
 * Filename: Section01_SimpleTemplate.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file was developed to demonstrate the 
 * flexibility and usefulness of templates.
 * It contains one function that is used with 
 * three different variable types
 * *******************************************/

#include<iostream>

/************************
 * Function name: printFunc
 * Precondition: T t_var
 * Postcondition: none
 * Takes in a template variable with a 
 * defined ostream operator, and 
 * prints its value to the screen.
 * **********************/
template<class T>
void printFunc(T t_var){
	std::cout << t_var << std::endl;
}

/************************************
 * Function name: main 
 * Precondition: none 
 * Postcondition: none
 * This is the main driver function. 
 * Creates an int, double, and char* 
 * and prints them to the ostream.
 * **********************************/
int main(){
    int i = 1842;
    double j = 3.14;
    char *k = "Go Irish!";
    printFunc(i); printFunc(j); printFunc(k);
}

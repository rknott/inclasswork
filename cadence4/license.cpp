/**********************************************
* File: license.cpp
* Author: Reilley Knott
* Email: rknott@nd.edu
*
* This function contains the coded solution to the 
* license plate question created with google that
* was asked in the Cadence 4 assignment
**********************************************/

#include <iostream>
#include <vector>
#include <string>
using namespace std;
#include <fstream>

void getletters(char[], int sz, vector<char>&);
void findword(vector<char>&, string, string&);

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* This is the main driver function for the license
* plate file 
********************************************/
int main() {
//open the input stream
ifstream inputfile("words.txt");

//create the license plate
int sz = 7;
char plate[] = {'B', 'R', '1', '2', '3', 'A', 'N'};

//create the vector to isolate the letters form the plate
vector<char> letters;
getletters(plate, sz, letters);

//the current shortest word will be stored here
string shortest = "abcdefghijklmnopqrstuvwxyz";

if (inputfile.is_open()) {
	while (inputfile.good()) {
		//input word from dictionary to be examined
		string word;
		//Input word from dictionary
		getline(inputfile, word);
		//See if the word has all the letters
		findword(letters, word, shortest);
	}
}

//Output the shortest word found
cout << shortest << endl;



return 0;
}

/********************************************
* Function Name  : getletters
* Pre-conditions : char plate[], int sz, vector<char>& letters
* Post-conditions: none
* This function works to isolate the letters from the rest of the
* license plate  
********************************************/
void getletters(char plate[], int sz, vector<char>& letters) {

	for (int i = 0; i <= sz; i++) {

		if (plate[i] >= 65 && plate[i] <= 90) {
			letters.push_back(plate[i]);
		}
	}
}

/********************************************
* Function Name  : findword
* Pre-conditions : vector<char>& letters, string word, string& shortest
* Post-conditions: none
* The function that will find a word containing all the letters in the
* letter vector and, if it is shorter than the word currently in "shortest"
* it will replace it. 
********************************************/
void findword(vector<char>& letters, string word, string& shortest) {

	//Look at first letter in "letters"
	for (auto i = letters.begin(); i != letters.end(); i++) {

		//initial case, letter not yet found
		bool found = false;

		//Attempt to find letter in "word"
		for (auto j = 0; j <= word.length(); j++) {

			//If found
			if (word[j] == (*i))
				found = true;
		}

		//if not found
		if (found == false)
			return;
	}

	//if all letters found
	if (word.length() < shortest.length() && word.length() > letters.size()) {
		shortest = word; }
		return;
}

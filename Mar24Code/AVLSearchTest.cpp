/**********************************************
* File: AVLAuthorTest.cpp
* Author: Reilley Knott
* Email: rknott@nd.edu
*
* This file contains the implementation and the main
* driver for the AVL Tree application of a small
* search engine containing urls and web names as elements 
* of the Search struct 
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

//Creation of Search Engine Struct
struct Search { 

	//Declaration of members of Struct
	std::string url;
	std::string webName;

	//Constructor
	Search(std::string url, std::string webName) : url(url), webName(webName) {}


	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Search& rhs
	* Post-conditions: bool
	* This function overloads the < operator to 
	* compare 2 urls (and potentially webNames)
	* to one another and determine if one is less
	* than the other for placement purposes within
	* the AVL Tree 
	********************************************/
	bool operator< (const Search& rhs) const { 
		
		if (url < rhs.url)
			return true;
		
			else if (url == rhs.url ) {
				if (webName < rhs.webName)
					return true;
			}

		return false; }

	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Search& rhs
	* Post-conditions: bool
	* This function overloads the == operator to 
	* determine if two urls are identical 
	********************************************/
	bool operator== (const Search& rhs) const { 
		
		if(url != rhs.url)
			return false;

			else { return true; }
	}

        /********************************************
        * Function Name  : operator>
        * Pre-conditions : const Search& rhs
        * Post-conditions: bool
        * This function overloads the > operator to 
        * compare 2 urls (and potentially webNames)
        * to one another and determine if one is greater
	* than the other for placement purposes within
        * the AVL Tree 
        ********************************************/
        bool operator> (const Search& rhs) const { 
    
                if (url > rhs.url)
                        return true;
    
                        else if (url == rhs.url ) { 
                                if (webName > rhs.webName)
                                        return true;
                        }   

                return false; }	

	/********************************************
	* Function Name  : operator<<
	* Pre-conditions : std::ostream& outStream, const Author& printAuth
	* Post-conditions: friend std::ostream&
	* The function which overloads the output operator and calls it
	* as a friend function in order to allow it to access the private
	* members of the struct
	********************************************/
	friend std::ostream& operator<<(std::ostream& outStream, const Search& printURL);
};


/********************************************
* Function Name  : operator<<
* Pre-conditions : std::ostream& outStream, const Author& printAuth
* Post-conditions: std::ostream&
* The function that specifies how an object of the Search struct will be
* visually outputted upon usage of the << operator
********************************************/
std::ostream& operator<<(std::ostream& outStream, const Search& printURL) {

	outStream << printURL.webName << "\n" << printURL.url << "\n\n";

	return outStream;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
  AVLTree<Search> avlSearch;

  //Constructing all the objects
  Search cse("https://cse.nd.edu", "Notre Dame CSE Homepage");
  Search archi("https://architecture.nd.edu", "Notre Dame Architecture Homepage");
  Search anthro("https://anthropology.nd.edu", "Notre Dame Anthropology Homepage");
  Search english("https://english.nd.edu", "Notre Dame English Homepage");
  Search pls("https://pls.nd.edu", "Notre Dame PLS Homepage");
  Search cheg("https://cbe.nd.edu", "Notre Dame Chemical Engineering Homepage");
  Search finance("https://mendoza.nd.edu", "Notre Dame Finance Homepage");
  Search scicomp("https://science.nd.edu", "Notre Dame Science-Computing Homepage");
  Search physics("https://physics.nd.edu", "Notre Dame Physics Homepage");
  Search math("https://math.nd.edu", "Notre Dame Math Homepage");

  //Inserting all the objects, with their order from an in-order traversal
  avlSearch.insert(cse);  //4
  avlSearch.insert(archi);  //2
  avlSearch.insert(anthro);  //1
  avlSearch.insert(english);  //5
  avlSearch.insert(pls);  //9
  avlSearch.insert(cheg);  //3
  avlSearch.insert(finance);  //7
  avlSearch.insert(scicomp);  //10
  avlSearch.insert(physics);   //8
  avlSearch.insert(math);  //6

  //Print Current Tree
  avlSearch.printTree();

  std::cout << "----\n\n" << endl;

  //CSE is at the root, deletion
  avlSearch.remove(cse);
  //Print Final Result
  avlSearch.printTree();
	
    return 0;
}
